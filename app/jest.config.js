const crypto = require('crypto');

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  globalSetup: '<rootDir>/__tests__/setup/dotenv-test-setup.js',
  setupFilesAfterEnv: ['<rootDir>/__tests__/setup/jest.setup.js'],
  testPathIgnorePatterns: [
    '/node_modules/',
    '/.next/',
    '/cypress/',
    '/__tests__/results/',
    '/__tests__/server/',
    '/__tests__/setup/',
    '/__tests__/utils/',
  ],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest',
  },
  transformIgnorePatterns: [
    '/node_modules/',
    '/.next/',
    '^.+\\.module\\.(css|sass|scss)$',
    '^.+\\.(css|sass|scss)$',
  ],
  moduleNameMapper: {
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
    '^.+\\.(css|sass|scss)$': 'identity-obj-proxy',
    '\\.svg$': '<rootDir>/mocks/svgMock.js',
  },
  globals: {
    // we must specify a custom tsconfig for tests because we need the typescript transform
    // to transform jsx into js rather than leaving it jsx such as the next build requires.  you
    // can see this setting in tsconfig.jest.json -> "jsx": "react"
    'ts-jest': {
      tsconfig: 'tsconfig.jest.json',
    },
    crypto,
  },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        suiteName: 'jest tests',
        outputDirectory: '__tests__/results',
        outputName: 'jest_junit_results.xml',
      },
    ],
  ],
  collectCoverage: true,
  coverageDirectory: '__tests__/results',
  coverageReporters: ['cobertura', 'lcov'],
};
