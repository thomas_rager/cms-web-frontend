import React, { FC } from 'react';

interface StageProps {
  title: string;
  abstract: string;
  buttonText: string;
  buttonLink: string;
  alignment: 'left' | 'right';
}

const Stage: FC<StageProps> = ({ title, abstract, buttonText, buttonLink, alignment }) => {
  return (
    <div>
      <h3>Stage - CMS-1</h3>
      <ul>
        <li>{title}</li>
        <li>{abstract}</li>
        <li>{buttonText}</li>
        <li>{buttonLink}</li>
        <li>{alignment}</li>
      </ul>
    </div>
  );
};

export default Stage;
