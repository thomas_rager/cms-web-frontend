import React, { FC } from 'react';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';

import style from '../styles/text.module.scss';

interface TextProps {
  title: string;
  text: text;
  alignment: 'left' | 'right';
}

/* eslint-disable react/no-danger */
const Text: FC<TextProps> = ({ title, text, alignment }) => {
  return (
    <div className={style.text}>
      <h3>Text - CMS-3</h3>
      <ul>
        <li>{title}</li>
        <li>
          <div dangerouslySetInnerHTML={{ __html: documentToHtmlString(text) }} />
        </li>
        <li>{alignment}</li>
      </ul>
    </div>
  );
};
/* eslint-enable react/no-danger */

export default Text;
