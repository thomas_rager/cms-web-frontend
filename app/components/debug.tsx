import React, { FC } from 'react';

import style from '../styles/debug.module.scss';

interface DebugProps {
  data?: Record<string, unknown>;
}

const Debug: FC<DebugProps> = ({ data }) => {
  return (
    <div className={style.debug}>
      <code>{JSON.stringify(data, null, '  ')}</code>
    </div>
  );
};

export default Debug;
