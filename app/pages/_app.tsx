import '../styles/globals.scss';
import { AppProps } from 'next/app';
import { NextPage } from 'next';

const MyApp: NextPage<AppProps> = ({ Component, pageProps }) => {
  return <Component {...pageProps} />;
};

export default MyApp;
