import { GetServerSideProps } from 'next';
import { pathOr } from 'ramda';
import Layout from '../../layout/layout';

import resolvePageData from '../../common/resolvePageData';
import { ComponentContentTypes } from '../../common/constants';

export const getServerSideProps: GetServerSideProps = async (context) => {
  const page = await resolvePageData({
    slug: context.query.slug,
    locale: 'de-CH',
  });

  return {
    props: {
      page,
    },
  };
};

const CMSPage = (props) => {
  const components = pathOr([], ['page', 'fields', 'components'], props);

  const pageContent = components.map((element, index) => {
    const componentProps = element.fields;
    const contentTypeId: string = element.sys.contentType.sys.id;
    const Component = ComponentContentTypes[contentTypeId];
    const key = `${contentTypeId}-${index}`;

    return <Component {...componentProps} key={key} />;
  });

  const {
    page: { fields },
  } = props;
  const { title, description } = fields;

  return (
    <Layout title={title} description={description}>
      {pageContent}
    </Layout>
  );
};

export default CMSPage;
