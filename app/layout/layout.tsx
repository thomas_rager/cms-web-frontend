import Head from 'next/head';
import React, { FC } from 'react';

interface LayoutProps {
  title: string;
  description: string;
}

const Layout: FC<LayoutProps> = (props) => {
  const { children, title, description } = props;
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
      </Head>
      <div>{children}</div>
    </div>
  );
};
export default Layout;
