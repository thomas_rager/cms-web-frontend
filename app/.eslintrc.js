module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es2021: true,
    jest: true,
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    project: ['./tsconfig.json'],
    sourceType: 'module',
  },
  ignorePatterns: [
    'node_modules/*',
    '.next/*',
    '.out/*',
    '.storybook/*',
    'storybook-static/*',
    'jest.config.js',
    'next.config.js',
    '.eslintrc.js'
  ], // We don't want to lint generated files nor node_modules
  settings: { react: { version: 'detect' } },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended', // TypeScript rules
    'airbnb-typescript', // AirBNB Rules
    'airbnb/hooks', // AirBNB React Hook Rules
    'plugin:react/recommended', // React rules
    'plugin:react-hooks/recommended', // React hooks rules
    'plugin:jsx-a11y/recommended', // Accessibility rules
    'plugin:jest/recommended', // Jest test rules
    'prettier', // Overrides some ESLint rules that conflict with Prettier
    'plugin:prettier/recommended', // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  rules: {
    'react/prop-types': 'off', // Because we are using TypeScript
    'react/react-in-jsx-scope': 'off', // Importing React is obsolete when using Next.js
    'react/jsx-props-no-spreading': 'off', // Allow for props spreading, for example in _app.tsx
    'linebreak-style': ['error', 'unix'], // For traceability reasons, we do not want unnecessary code changes due to line break styles
    'jsx-a11y/anchor-is-valid': 'off', // This rule is not compatible with Next.js's <Link /> components
    'import/no-extraneous-dependencies': [
      // Test dependencies must not be part of the project's dependencies but may be a devDependency instead
      'error',
      {
        devDependencies: [
          '__tests__/**',
          '**/*.test.ts',
          '**/*.test.tsx',
          '**/*.stories.tsx',
        ],
      },
    ],
    'jest/expect-expect': 'off', // Not required for Cypress tests
    '@typescript-eslint/no-unused-expressions': [
      'error',
      { allowShortCircuit: true },
    ], // allow short circuit, eg. to call a callback function which may or may not be set by the enclosed component
    'react/require-default-props': 'off', // because they will be deprecated for function components (https://github.com/reactjs/rfcs/pull/107)
  },
};
