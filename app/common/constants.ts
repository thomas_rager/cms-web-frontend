import Stage from '../components/stage';
import Text from '../components/text';

export const PageContentType = 'page';

export const ComponentContentTypes = {
  stage: Stage,
  text: Text,
};
