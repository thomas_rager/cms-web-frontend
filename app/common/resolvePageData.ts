import { createClient } from 'contentful';
import { PageContentType } from './constants';

type ResolvePageParams = {
  slug: string;
  locale: string;
};

const client = createClient({
  space: process.env.space,
  accessToken: process.env.accessToken,
});

const resolvePageData = async (params: ResolvePageParams) :any => {
  const result = await client.getEntries({
    content_type: PageContentType,
    'fields.slug': params.slug,
    locale: params.locale,
  });

  if (result.items) {
    return result.items[0];
  }
  return { error: 'NOT_FOUND' };
};

export default resolvePageData;
